import argparse

data = {}


def read_input(fn):
    """
    Read input file and fill the dictionary data as:
    key: filename
    element: weight
    """
    for line in open(fn):
        list_elements = line.split()
        file_from = list_elements[0]
        file_to = list_elements[1]
        weight_data = float(list_elements[2]) / 100
        """
        data structure
        filename_from filename_to weight
        """
        print("Reading {}".format(file_from))
        data[file_from] = [-weight_data, [line.rstrip('\n') for line in open(file_from)]]
        print("Reading {}".format(file_to))
        data[file_to] = [weight_data, [line.rstrip('\n') for line in open(file_to)]]


def main():
    """
Main routine
"""


parser = argparse.ArgumentParser()
#
parser.add_argument("-i", "--input", help="Name of the input file")
parser.add_argument("-o", "--output", help="Name of the output file")

# Get the args
args = parser.parse_args()

# Get i/o filenames
input_file = args.input
output_file = args.output

# Read input file and cube files within
read_input(input_file)
fnout = open(output_file, "w")

# Process data
header = []
result = []
first_run = True
for key in data:
    weight = float(data[key][0])
    lines = data[key][1]
    print("Adding {0} x {1}".format(key, weight))
    j = 0
    for i in range(len(lines)):
        if i < 1434:
            if first_run:
                # Fill the header
                header.append(lines[i])
            else:
                pass
        else:
            for val in lines[i].split():
                if first_run:
                    result.append(weight * pow(float(val), 2))
                else:
                    result[j] = result[j] + weight * pow(float(val), 2)
                j = j + 1
    first_run = False

# print("{0:13.5E}".format(c1 * pow(float(l1[j]), alpha) + c2 * pow(float(l2[j]), alpha)), end='')

for line in header:
    fnout.write(line)
    fnout.write("\n")
nvaltot = 161
nvallig = 6
for j in range(len(result)):
    fnout.write("{0:13.5E}".format(result[j]))
    nvaltot = nvaltot - 1
    nvallig = nvallig - 1
    if nvaltot == 0:
        fnout.write("\n")
        nvaltot = 161
        nvallig = 6
    if nvallig == 0:
        fnout.write("\n")
        nvallig = 6

if __name__ == "main":
    main()
